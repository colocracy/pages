# Pemerintah kolaboratif adalah masa depan
## Pengantar
Bayangkan sebuah sistem pemerintahan yang merupakan cermin dari orang-orang yang diwakilinya, yang menghormati perbedaan rakyatnya dan menyeimbangkan inklusivitas. Sebuah sistem pemerintahan direkayasa untuk kolaborator bukan musuh. Di mana uang, partai atau ideologi bukanlah prasyarat untuk diangkat ke badan legislatif dan yang dicabut haknya memiliki perwakilan proporsional. Bayangkan sebuah sistem pemerintahan kebal terhadap pengaruh berlebihan kelompok lobi monied, politik memecah belah dan kepentingan sempit. Sebuah sistem direkayasa sebagai pengganti plugin atau hibrida untuk sistem pemerintahan lainnya. Sebuah sistem yang dapat diimplementasikan dari akar rumput tanpa pemberontakan, pembangkangan sipil atau protes. Pemerintahan untuk banyak dan sedikit; kiri dan kanan, kaya dan miskin, seimbang secara demografis, bebas secara ideologis, pemerintahan untuk kita semua.
## Manifesto
[Baca The Colocracy Manifesto](manifesto.md) dan temukan bagaimana kolaborasi—*konferensi perbedaan* yang mendefinisikan setiap partikel, atom, dan molekul—juga mendefinisikan kontrak sosial setiap keluarga, desa, dan bangsa. [Baca manifesto sekarang](manifesto.md).
## Kontak
Hubungi mail@colocracy.org atau pesan *colocracy* di [Jami](https://jami.net/).
## Menyumbang
1. [Berkolaborasi di Codeberg](https://codeberg.org/colocracy);
2. [Terhubung di Mastodon](https://mastodon.social/web/@colocracy);
3. [Ikuti kami di Twitter](https://twitter.com/colocracy);
4. Bantu dukung pembuat colocracy dengan menyumbang [IOTA](https://www.iota.org/):
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```