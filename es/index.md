# El gobierno colaborativo es el futuro
## Introducción
Imagine un sistema de gobierno que sea un espejo de la gente que representa, que respete la diferencia de su gente y equilibre la inclusión. Un sistema de gobierno diseñado para colaboradores, no para adversarios. Donde el dinero, el partido o la ideología no son requisitos previos para el nombramiento a la legislatura y los privados de sus derechos tienen representación proporcional. Imagine un sistema de gobierno inmune a la influencia excesiva de grupos de presión adinerados, políticas divisivas e intereses estrechos. Un sistema diseñado como reemplazo de complemento o híbrido para otros sistemas de gobierno. Un sistema que se pueda implementar desde la base sin rebelión, desobediencia civil o protesta. Un gobierno para muchos y pocos; la izquierda y la derecha, los ricos y los pobres, demográficamente equilibrados, ideológicamente libres, un gobierno para todos.
## Manifiesto
[Lea El Manifiesto de la Colocracia](manifesto.md) y descubra cómo la colaboración, la *conferencia de la diferencia* que define cada partícula, átomo y molécula, también define el contrato social de cada familia, pueblo y nación. [Lea el manifiesto ahora](manifesto.md).
## Contacto
Comuníquese con mail@colocracy.org o envíe un mensaje a *colocracy* en [Jami](https://jami.net/).
## Contribuir
1. [Colaborar en Codeberg](https://codeberg.org/colocracy);
2. [Conéctate en Mastodon](https://mastodon.social/web/@colocracy);
3. [Síguenos en Twitter](https://twitter.com/colocracy);
4. Apoya al creador de la colocracia donando [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```