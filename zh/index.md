# 協作政府是未來
## 緒言
想象一個反映其所代表的人民、尊重人民差異並平衡包容性的政府體系。一個爲合作者而不是對手設計的政府體系。金錢、政黨或意識形態不是任命立法機構成員的先決條件、被剝奪權利的人擁有比例代表制。想象一個不受金錢遊說團體、分裂政治和狹隘利益過度影響的政府體系。一個被設計爲其他政府體系的插件替代品或混合體的系統。一個可以在沒有叛亂、非暴力反抗或抗議的情況下從基層實施的系統。一個多數人和少數人的政府、左派和右派、富人和窮人、人口平衡、意識形態自由、一個我們所有人的政府。
## 宣言
[閱讀《貴族宣言》](manifesto.md)、發現協作——定義每一個粒子、原子和分子的差異會議——如何也定義了每個家庭、村莊和國家的社會契約。[現在就閱讀宣言](manifesto.md)。
## 聯繫方式
聯繫 [mail@colocracy.org](https://mailto:mail@colocracy.org) 或消息 *colocracy* 上[Jami](https://jami.net/)。
## 貢獻
1. [在Codeberg上進行協作](https://codeberg.org/colocracy)、
2. [在乳齒象上連接](https://mastodon.social/web/@colocracy)、
3. [在Twitter上關注我們](https://twitter.com/colocracy)、
4. 通過捐贈[IOTA](https://www.iota.org/)來幫助支持哥倫比亞的創造者：
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```
