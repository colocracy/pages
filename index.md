# Collaborative government is the future
## Introduction
Imagine a system of government that is a mirror of the people it represents, that respects the difference of its people and balances inclusiveness. A system of government engineered for collaborators not adversaries. Where money, party or ideology are not prerequisites for appointment to the legislature and the disenfranchised have proportional representation. Imagine a system of government immune to the excessive influence of monied lobby groups, divisive politics and narrow interests. A system engineered as a plugin replacement or hybrid for other systems of government. A system that can be implemented from grassroots without rebellion, civil disobedience or protest. A government for the many and the few; the left and the right, the rich and the poor, demographically balanced, ideologically-free, a government for all of us.
## Manifesto
[Read The Colocracy Manifesto](manifesto.md) and discover how collaboration—the *conference of difference* that defines every particle, atom and molecule—also defines the social contract of every family, village and nation. [Read the manifesto now](manifesto.md).
## Contact
Contact mail@colocracy.org or message *colocracy* on [Jami](https://jami.net/).
## Contribute
1. [Collaborate on Codeberg](https://codeberg.org/colocracy); 
2. [Connect on Mastodon](https://mastodon.social/web/@colocracy);
3. [Follow us on Twitter](https://twitter.com/colocracy);
4. Help support colocracy's creator by donating [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```


