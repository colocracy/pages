# Governo colaborativo é o futuro
## Introdução
Imagine um sistema de governo que seja um espelho do povo que representa, que respeite a diferença de seu povo e equilibre a inclusão. Um sistema de governo projetado para colaboradores e não adversários. Onde dinheiro, partido ou ideologia não são pré-requisitos para a nomeação para o legislativo e os desprivilegiados têm representação proporcional. Imagine um sistema de governo imune à influência excessiva de grupos de lobby endinheirados, políticas divisórias e interesses estreitos. Um sistema projetado como um substituto de plug-in ou híbrido para outros sistemas de governo. Um sistema que pode ser implementado desde a base sem rebelião, desobediência civil ou protesto. Um governo para muitos e poucos; a esquerda e a direita, os ricos e os pobres, demograficamente equilibrados, ideologicamente livres, um governo para todos nós.
## Manifesto
[Leia The Colocracy Manifesto](manifesto.md) e descubra como a colaboração—a *conferência da diferença* que define cada partícula, átomo e molécula—também define o contrato social de cada família, aldeia e nação. [Leia o manifesto agora](manifesto.md).
## Contato
Entre em contato com mail@colocracy.org ou envie uma mensagem para *colocracy* em [Jami](https://jami.net/).
## Contribuir
1. [Colabore no Codeberg](https://codeberg.org/colocracia);
2. [Conecte-se no Mastodon](https://mastodon.social/web/@colocracy);
3. [Siga-nos no Twitter](https://twitter.com/colocracia);
4. Ajude a apoiar o criador do Colocracy através do [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```