# O Manifesto da Colocracia
### Destinado, nós, o povo, em colaboração, equilibrados em nossa imagem, nascemos de novo.
*Sociedade* é uma 'condição de partilha', uma *conferência* da *diferença*, uma 'condição de suportar juntos', transformando a 'condição de suportar juntos'. A conferência da diferença é a primeira epístola do ser, a gênese de cada partícula, átomo e molécula em nosso universo. Toda existência é uma conferência da diferença. E embora muitas espécies sociais amplificam a conferência na cooperação, apenas os humanos sintetizam a diferença na colaboração. A colaboração é o nosso superpoder. Sociedade, é a nossa colaboração. Então, vamos colaborar. Que todas as nossas diferenças sejam representadas. Que cada legislatura espelhe nossa diferença. Que sejam homens e mulheres em equilíbrio, a diferença de idades em equilíbrio e a diferença de meios em equilíbrio. Pois assim como os átomos regulam forças particulares de diferença para conservar o equilíbrio e a estabilidade, a sociedade também deve fazê-lo, pois ela também está sujeita à condição universal de ser. Que nossas legislaturas reflitam quem realmente somos. Não falemos de sociedade. Vamos criá-lo, respirá-lo, vivê-lo. Que cada legislador traga sua própria diferença sabendo que a colaboração não pode ocorrer sem ela. Que cada voz seja ouvida. E que aqueles que argumentam contra o direito de serem ouvidos tenham esse respeito. Que cada um de nós considere a diferença dos outros como gostaríamos que considerassem a nossa. Que esta seja a nossa regra de ouro. E, portanto, nossas legislaturas sempre refletem o equilíbrio das diferenças na sociedade, permitindo que os cidadãos sejam selecionados para um único mandato de 4 anos por meio de seleção demográfica proporcional– não eleição competitiva. E deixe que 25% dos legisladores entrem e saiam da legislatura anualmente, de modo a manter a diferença de novos e experientes na proporção. E que cada legislador, via escola secundária ou terciária, seja certificado no processo de governo colaborativo para que possa mediar as diferenças da sociedade com responsabilidade.de 'regra colaborativa' *colocracia*. E que não se manifeste por meio de protesto, rebelião ou cultura de reclamação, pois essas são as armas da facção e da divisão. Que este manifesto fale a cada um de nós individualmente e que o general se determine. Pois assim como a diferença dos indivíduos preenche a 'condição de compartilhar' que é a *sociedade*, também essas diferenças devem ser conservadas, equilibradas e refletidas no governo, pois este é o contrato social.
## Direito autoral
*O Manifesto da Colocracia* é copyright (c) John Mackay e licenciado sob a [Atribuição-CompartilhaIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.pt) (CC BY- SA 4.0).
## Contato
Entre em contato com mail@colocracy.org ou envie uma mensagem para *colocracy* em [Jami](https://jami.net/).
## Contribuir
1. [Colabore no Codeberg](https://codeberg.org/colocracia);
2. [Conecte-se no Mastodon](https://mastodon.social/web/@colocracy);
3. [Siga-nos no Twitter](https://twitter.com/colocracia);
4. Ajude a apoiar o criador do Colocracy através do [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```

## Baixe
[Baixe a versão `pt` O Manifesto da Colocracia](manifesto-pt.pdf) em Portable Document Format (PDF), formatado para A4.

Observação! As versões em PDF do *O Manifesto da Colocracia* e suas traduções são formatadas para melhor exibição usando [Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans), uma fonte global gentilmente doada pelo Google sob o [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL) (OFL) com suporte para mais de 3.000 idiomas escritos.