# Il governo collaborativo è il futuro
## Introduzione
Immaginate un sistema di governo che sia lo specchio delle persone che rappresenta, che rispetti le differenze dei suoi cittadini e che bilanci l'inclusione. Un sistema di governo progettato per i collaboratori e non per gli avversari. Dove il denaro, il partito o l'ideologia non sono prerequisiti per la nomina alla legislatura e i non eletti hanno una rappresentanza proporzionale. Immaginate un sistema di governo immune dall'eccessiva influenza di gruppi di pressione monetizzati, politiche divisive e interessi ristretti. Un sistema progettato come sostituto o ibrido di altri sistemi di governo. Un sistema che può essere implementato dal basso senza ribellioni, disobbedienza civile o proteste. Un governo per i molti e per i pochi, per la sinistra e per la destra, per i ricchi e per i poveri, demograficamente equilibrato, ideologicamente libero, un governo per tutti noi.
## Manifesto
[Leggete il Manifesto della Colocrazia](manifesto.md) e scoprite come la collaborazione - la *conferenza delle differenze* che definisce ogni particella, atomo e molecola - definisce anche il contratto sociale di ogni famiglia, villaggio e nazione. [Leggi ora il manifesto](manifesto.md).
## Conttato
Contattare mail@colocracy.org o inviare un messaggio a *colocracy* su [Jami](https://jami.net/).
## Contribuire
1. [Collaborare su Codeberg](https://codeberg.org/colocracy); 
2. [Unisciti a noi su Mastodon](https://mastodon.social/web/@colocrazia);
3. [Seguiteci su Twitter](https://twitter.com/colocracy);
4. Aiuta a sostenere il creatore di colocracy con una donazione [IOTA](https://www.iota.org/):
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```