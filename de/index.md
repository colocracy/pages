# Kollaborativ Regierung ist die Zukunft
## Einleitung
Stellen Sie sich ein Regierungssystem vor, das ein Spiegel der Menschen ist, die es repräsentiert, das die Unterschiede seiner Menschen respektiert und die Inklusivität ausbalanciert. Ein Regierungssystem, das für Kollaborateure entwickelt wurde, nicht für Gegner. Wo Geld, Partei oder Ideologie keine Voraussetzungen für die Ernennung zum Gesetzgeber sind und die Entrechteten eine proportionale Vertretung haben. Stellen Sie sich ein Regierungssystem vor, das gegen den übermäßigen Einfluss von Geldlobbygruppen, spalterischer Politik und engstirnigen Interessen immun ist. Ein System, das als Plugin-Ersatz oder Hybrid für andere Regierungssysteme entwickelt wurde. Ein System, das von der Basis aus ohne Rebellion, zivilen Ungehorsam oder Protest umgesetzt werden kann. Eine Regierung für die Vielen und die Wenigen; links und rechts, reich und arm, demographisch ausgewogen, ideologiefrei, eine Regierung für uns alle.
## Manifest
[Lesen Sie Das Kolokratie-Manifest](manifesto.md) und entdecken Sie, wie Kollaboration – die *Konferenz der Differenz*, die jedes Teilchen, Atom und Molekül definiert – auch den Gesellschaftsvertrag jeder Familie, jedes Dorfes und jeder Nation definiert. [Lesen Sie jetzt das Manifest](manifesto.md).
## Kontakt
Kontaktieren Sie mail@colocracy.org oder senden Sie eine Nachricht an *colocracy* auf [Jami](https://jami.net/).
## Beitung
1. [Bei Codeberg Kollaborieren](https://codeberg.org/colocracy);
2. [Mit Mastodon verbinden](https://mastodon.social/web/@colocracy);
3. [Folgen Sie uns auf Twitter](https://twitter.com/colocracy);
4. Helfen Sie mit, den Schöpfer von Kolokratie zu unterstützen, indem Sie [IOTA](https://www.iota.org/) spenden:
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```