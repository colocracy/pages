# Le gouvernement collaboratif est l'avenir
## l'introduction
Imaginez un système de gouvernement qui soit le miroir du peuple qu'il représente, qui respecte la différence de son peuple et qui équilibre l'inclusivité. Un système de gouvernement conçu pour des collaborateurs et non des adversaires. Là où l'argent, le parti ou l'idéologie ne sont pas des conditions préalables à la nomination à l'Assemblée législative, les personnes privées de leurs droits bénéficient d'une représentation proportionnelle. Imaginez un système de gouvernement à l'abri de l'influence excessive des groupes de pression riches, des politiques qui divisent et des intérêts étroits. Un système conçu comme un plug-in de remplacement ou hybride pour d'autres systèmes de gouvernement. Un système qui peut être mis en œuvre à partir de la base sans rébellion, désobéissance civile ou protestation. Un gouvernement pour le grand nombre et le petit nombre ; la gauche et la droite, les riches et les pauvres, démographiquement équilibrés, idéologiquement libres, un gouvernement pour nous tous.
## Manifeste
[Lisez le manifeste de la colocratie](manifesto.md) et découvrez comment la collaboration - la *conférence de la différence* qui définit chaque particule, atome et molécule - définit également le contrat social de chaque famille, village et nation. [Lire le manifeste maintenant](manifesto.md).
## Contactez
Contactez mail@colocracy.org ou envoyez un message *colocracy* sur [Jami](https://jami.net/).
## Contribuer
1. [Collaborer sur Codeberg](https://codeberg.org/colocracy) ;
2. [Se connecter sur Mastodon](https://mastodon.social/web/@colocracy);
3. [Suivez-nous sur Twitter](https://twitter.com/colocracy);
4. Aidez à soutenir le créateur de Colocracy via [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```