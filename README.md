# README
## Welcome
Welcome to the `main` branch of colocracy.org. 

`Attention!` You may be interacting with this information at a location other than its original source at https://codeberg.org/colocracy/pages. Check the security certificate in your browser's address bar for details.
## Copyright
Unless otherwise specified, information provided on this repository and its branches are the copyright (c) of John Mackay and licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-sa/4.0/legalcode) (CC BY-SA 4.0).
## Contact
Contact mail at colocracy.org or message *colocracy* on [Jami](https://jami.net/).
## Contribute
1. [Collaborate on Codeberg](https://codeberg.org/colocracy); 
2. [Connect on Mastodon](https://mastodon.social/web/@colocracy);
3. [Follow us on Twitter](https://twitter.com/colocracy);
4. Support colocracy's creator by donating [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```