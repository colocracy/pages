# The Colocracy Manifesto
### Destined, we the people, in collaboration, balanced in our image are born, anew.
*Society* is a 'condition of sharing', a *conference* of *difference*, a 'condition of bearing together', transforming the 'condition of bearing apart'. The conference of difference is the first epistle of being, the genesis of every particle, atom and molecule in our universe. All existence is a conference of difference. And though many social species amplify conference in cooperation, only humans synthesize difference in collaboration. Collaboration is our superpower. Society, is our collaboration. So, let us collaborate. Let our every difference be represented. Let every legislature mirror our difference. Let it be men and women in balance, the difference of ages in balance and the difference of means in balance. For just as atoms regulate particular forces of difference to conserve balance and stability, so must society, for it too is subject to the universal condition of *being*. Let our legislatures reflect who we really are. Let us not talk of society. Let us create it, breath it, live it. Let every legislator bring their own difference knowing that collaboration cannot occur without it. Let every voice be heard. And let those who argue against the right to be heard be afforded that respect. Let every one of us consider the difference of others as we would want them to consider ours. Let this be our golden rule. And so our legislatures always reflect the balance of difference in society, let citizens be selected to a single 4 year term via proportional demographic selection—not competitive election. And let 25% of legislators rotate in and out of the legislature annually so as to maintain the difference of new and seasoned in proportion. And let every legislator, via secondary or tertiary school, be certified in the process of collaborative government so they can mediate society's differences responsibly. And let us name this system of 'collaborative rule' *colocracy*. And let it not manifest through protest, rebellion or culture of complaint, for these are the weapons of faction and division. Let this manifesto speak to each of us individually and let the general will determine itself. For just as the difference of individuals fulfill the 'condition of sharing' that is *society*, so too must those differences be conserved, balanced and reflected in government, for this is the social contract.
## Copyright
*The Colocracy Manifesto* is copyright (c) John Mackay and licensed under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-sa/4.0/legalcode) (CC BY-SA 4.0).
## Contact
Contact mail@colocracy.org or message *colocracy* on [Jami](https://jami.net/).
## Contribute
1. [Collaborate on Codeberg](https://codeberg.org/colocracy); 
2. [Connect on Mastodon](https://mastodon.social/web/@colocracy);
3. [Follow us on Twitter](https://twitter.com/colocracy);
4. Support colocracy's creator by donating [IOTA](https://www.iota.org/): 
```
iota1qq365hf9pfgywy9r4fy6qkngp3elqgeqerv03qlth408ncdnwhqfsdqlul0
```
## Download
1. [Download the `en-US` version of The Colocracy Manifesto](colocracy-manifesto-en-us.pdf) in Portable Document Format (PDF), formatted to US Letter.
2. [Download the `en-UK` version of The Colocracy Manifesto](colocracy-manifesto-en-uk.pdf) in Portable Document Format (PDF), formatted to A4.

Note! PDF versions of *The Colocracy Manifesto* and its translations are formatted for best display using [Noto Sans](https://fonts.google.com/noto/specimen/Noto+Sans), a global font kindly donated by Google under the [Open Font License](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL) (OFL) with support for more than 3000 written languages.

